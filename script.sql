USE [master]
GO
/****** Object:  Database [Flights]    Script Date: 11/22/2017 2:21:36 PM ******/
CREATE DATABASE [Flights]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Flights', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Flights.mdf' , SIZE = 204800KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Flights_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Flights_log.ldf' , SIZE = 139264KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Flights] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Flights].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Flights] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Flights] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Flights] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Flights] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Flights] SET ARITHABORT OFF 
GO
ALTER DATABASE [Flights] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Flights] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Flights] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Flights] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Flights] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Flights] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Flights] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Flights] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Flights] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Flights] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Flights] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Flights] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Flights] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Flights] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Flights] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Flights] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Flights] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Flights] SET RECOVERY FULL 
GO
ALTER DATABASE [Flights] SET  MULTI_USER 
GO
ALTER DATABASE [Flights] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Flights] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Flights] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Flights] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Flights] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Flights', N'ON'
GO
ALTER DATABASE [Flights] SET QUERY_STORE = OFF
GO
USE [Flights]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Flights]
GO
/****** Object:  User [U185499]    Script Date: 11/22/2017 2:21:36 PM ******/
CREATE USER [U185499] FOR LOGIN [U185499] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [U185180]    Script Date: 11/22/2017 2:21:36 PM ******/
CREATE USER [U185180] FOR LOGIN [U185180] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [U123070]    Script Date: 11/22/2017 2:21:36 PM ******/
CREATE USER [U123070] FOR LOGIN [U123070] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [U185499]
GO
ALTER ROLE [db_datareader] ADD MEMBER [U185180]
GO
ALTER ROLE [db_datareader] ADD MEMBER [U123070]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [U123070]
GO
/****** Object:  Table [dbo].[Flights_tbl]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flights_tbl](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CityPar] [varchar](50) NOT NULL,
	[CountryO] [varchar](10) NOT NULL,
	[CountryD] [varchar](10) NOT NULL,
	[AL] [varchar](10) NOT NULL,
	[FNR] [varchar](10) NOT NULL,
	[Leg] [int] NOT NULL,
	[Origin] [varchar](10) NOT NULL,
	[Destination] [varchar](10) NOT NULL,
	[STD] [time](7) NOT NULL,
	[STA] [time](7) NOT NULL,
	[Block] [float] NOT NULL,
	[Code] [varchar](10) NULL,
	[I_K] [varchar](10) NOT NULL,
	[H_D] [varchar](10) NOT NULL,
	[Aircraft] [varchar](10) NOT NULL,
	[FltDate] [date] NOT NULL,
	[ImportFile] [varchar](50) NULL,
	[PRICE] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_DEPARTURES1]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_DEPARTURES1](
	[ID] [nvarchar](255) NULL,
	[Origin] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Weekdays] [nvarchar](255) NULL,
	[Departure] [time](7) NULL,
	[Arrival] [time](7) NULL,
	[Flight No] [nvarchar](255) NULL,
	[Period of operation] [nvarchar](255) NULL,
	[ValidFrom] [nvarchar](255) NULL,
	[ValidTo] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL,
	[DIFF] [numeric](23, 10) NULL,
	[PRICE] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NEW_ARRIVALS]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NEW_ARRIVALS](
	[ID] [nvarchar](255) NULL,
	[Origin] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Weekdays] [nvarchar](255) NULL,
	[Departure] [time](7) NULL,
	[Arrival] [time](7) NULL,
	[Flight No] [nvarchar](255) NULL,
	[Period of operation] [nvarchar](255) NULL,
	[ValidFrom] [nvarchar](255) NULL,
	[ValidTo] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL,
	[DIFF] [numeric](22, 10) NULL,
	[price] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Airlines]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Airlines](
	[Iata] [varchar](10) NOT NULL,
	[AL_Name] [varchar](50) NOT NULL,
	[InsertDate] [datetime] NOT NULL,
 CONSTRAINT [pk_Iata] PRIMARY KEY CLUSTERED 
(
	[Iata] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[one_stop]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO














CREATE VIEW [dbo].[one_stop] 
AS

    SELECT A.COUNTRYO,A.COUNTRYD,A.AL,A.FNR,A.ORIGIN ORIGIN2,A.DESTINATION CITY_DESTINATION,A.STD,A.STA,A.BLOCK,A.FLTDATE,((DATEPART(DW,FLTDATE) + 5) % 7 + 1) WKD,
	ISNULL(A.PRICE,0) PRICE1,ISNULL(B.PRICE,0) PRICE2 ,(ISNULL(A.PRICE,0) + ISNULL(B.PRICE,0))  PRICE_TOT,' = ' SPLIT,B.ORIGIN CITY_ORIGIN,B.DESTINATION DESTINATION1,B.WEEKDAYS,B.DEPARTURE,B.ARRIVAL,B.[FLIGHT NO],
	CONVERT(VARCHAR(10), CONVERT(DATE, B.VALIDFROM, 103), 120) VALIDFROM,
	CONVERT(VARCHAR(10), CONVERT(DATE, B.VALIDTO, 103), 120) VALIDTO,B.AL_IATA,B.DIFF,

	CASE WHEN (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 )  < 0 THEN (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 ) + 24
   ELSE (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 ) END TRANZ,

   (BLOCK + DIFF + 
   (CASE WHEN (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 )  < 0 THEN (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 ) + 24
  ELSE (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 ) END)
   ) TOT,C.AL_NAME


	FROM  [FLIGHTS].[DBO].[FLIGHTS_TBL] A

	 LEFT JOIN [FLIGHTS].[DBO].[TIA_DEPARTURES1] B 

		ON B.WEEKDAYS LIKE '%'+ CAST(((DATEPART(DW,FLTDATE) + 5) % 7 + 1) AS VARCHAR) +'%'

			AND FLTDATE BETWEEN  CONVERT(VARCHAR(10), CONVERT(DATE, VALIDFROM, 103), 120) AND CONVERT(VARCHAR(10), CONVERT(DATE, VALIDTO, 103), 120)

			AND  A.ORIGIN = B.DESTINATION 

	

   LEFT JOIN FLIGHTS.DBO.AIRLINES C ON B.AL_IATA = C.IATA

	
			


    WHERE   A.BLOCK < 3

	AND B.DIFF < 3

	AND (BLOCK + DIFF + 
  (CASE WHEN (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 )  < 0 THEN (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 ) + 24
 ELSE (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 ) END)
  )   < 6

  AND 
  	CASE WHEN (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 )  < 0 THEN (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 ) + 24
   ELSE (DATEDIFF(SS,ARRIVAL,STD) /60.0 / 60.0 ) END > 0.5
 
  AND  A.STD > B.ARRIVAL


GO
/****** Object:  View [dbo].[ALL_AIRLINES]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create view [dbo].[ALL_AIRLINES] as

SELECT *
	FROM 
(

SELECT  CAST([COUNTRYO] AS VARCHAR) [COUNTRYO] 
,CAST([COUNTRYD] AS VARCHAR)  [COUNTRYD]
,[AL],CAST([FNR] AS VARCHAR) [FNR] ,[ORIGIN2]
,[CITY_DESTINATION]
,[STD],[STA],[BLOCK],[FLTDATE] ,CAST([WKD] AS VARCHAR) [WKD]
,CAST([PRICE1] AS varchar)  [PRICE1]
,CAST([PRICE2] AS varchar) [PRICE2]
,CAST([PRICE_tot] AS varchar) [PRICE_tot]
,'=' [SPLIT]
,[CITY_ORIGIN] ,[DESTINATION1]
,CAST([WEEKDAYS] AS VARCHAR) [WEEKDAYS],[DEPARTURE]
,[ARRIVAL] ,CAST([FLIGHT NO] AS VARCHAR) [FLIGHT NO]
,[VALIDFROM],[VALIDTO]
,[AL_IATA],CAST([DIFF] AS VARCHAR) [DIFF]
,CAST([tranz] AS VARCHAR) [tranz],CAST([TOT]AS VARCHAR)[TOT],
CAST([AL_NAME]AS VARCHAR) [AL_NAME]
FROM [FLIGHTS].[DBO].one_stop
WHERE PRICE_TOT <> 0

UNION ALL

 SELECT '' [COUNTRYO] ,'' [COUNTRYD]
,AL_IATA [AL], [FLIGHT NO] [FNR] ,''[ORIGIN2]
,[DESTINATION] [CITY_DESTINATION]
,[DEPARTURE] [STD],[ARRIVAL] [STA]
,[DIFF] [BLOCK],'' [FLTDATE] ,CAST(''  AS INT) [WKD]
,ISNULL(PRICE,0) [PRICE1]
,'0'[PRICE2]
,ISNULL(PRICE,0) + 0 [PRICE_TOT]
,'=' [SPLIT]
,[ORIGIN] [CITY_ORIGIN] ,''[DESTINATION1]
,[WEEKDAYS] ,[DEPARTURE]
,[ARRIVAL] ,[FLIGHT NO]
,[VALIDFROM],[VALIDTO]
,[AL_IATA],[DIFF]
,''[tranz],''[TOT],AL_IATA [AL_NAME]
FROM [FLIGHTS].[DBO].[NEW_ARRIVALS]

UNION all

 SELECT '' [COUNTRYO] ,''[COUNTRYD]
,AL_IATA [AL], [FLIGHT NO][FNR] ,''[ORIGIN2]
,[DESTINATION] [CITY_DESTINATION]
,[DEPARTURE] [STD],[ARRIVAL] [STA]
,[DIFF] [BLOCK],'' [FLTDATE] ,'' [WKD]
,ISNULL(PRICE,0) [PRICE1]
,'0'[PRICE2]
,ISNULL(PRICE,0) + 0 [PRICE_TOT]
,'='[SPLIT]
,[ORIGIN] [CITY_ORIGIN] ,''[DESTINATION1]
,[WEEKDAYS] ,[DEPARTURE]
,[ARRIVAL] ,[FLIGHT NO]
,[VALIDFROM],[VALIDTO]
,[AL_IATA],[DIFF]
,''[tranz],''[TOT],AL_IATA [AL_NAME]
FROM  [FLIGHTS].[DBO].TIA_DEPARTURES1 

UNION all
 SELECT [COUNTRYO] ,[COUNTRYD]
,[AL],[FNR] ,''[ORIGIN2]
,DESTINATION [CITY_DESTINATION]
,[STD],[STA],[BLOCK],[FLTDATE] ,''[WKD]
,ISNULL(PRICE,0) [PRICE1]
,'0'[PRICE2]
,ISNULL(PRICE,0) + 0 [PRICE_TOT]
,'='[SPLIT]
,ORIGIN [CITY_ORIGIN] ,''[DESTINATION1]
,CAST(DATEPART(DW,FLTDATE) AS VARCHAR) [WEEKDAYS] 
,STD [DEPARTURE]
,STA [ARRIVAL] , CAST(FNR AS VARCHAR) [FLIGHT NO]
,'' [VALIDFROM],'' [VALIDTO]
,AL [AL_IATA],BLOCK [DIFF]
,''[tranz],''[TOT],AL [AL_NAME]
FROM FLIGHTS_TBL

 ) X
GO
/****** Object:  Table [dbo].[Airport]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Airport](
	[AIRPORT_ID] [int] NOT NULL,
	[NAME] [varchar](max) NULL,
	[CITY] [varchar](max) NULL,
	[COUNTRY] [varchar](max) NULL,
	[IATA] [varchar](max) NULL,
	[ICAO] [varchar](max) NULL,
	[LATITUDE] [varchar](max) NULL,
	[LONGITUDE] [varchar](max) NULL,
	[ALTITUDE] [varchar](max) NULL,
	[TIMEZONE] [varchar](max) NULL,
	[DST] [varchar](max) NULL,
	[TZ_DATABASE_TIMEZONE] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AIRPORT2]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AIRPORT2](
	[AIRPORT_ID] [int] NOT NULL,
	[NAME] [varchar](max) NULL,
	[CITY] [varchar](max) NULL,
	[COUNTRY] [varchar](max) NULL,
	[IATA] [varchar](max) NULL,
	[ICAO] [varchar](max) NULL,
	[LATITUDE] [varchar](max) NULL,
	[LONGITUDE] [varchar](max) NULL,
	[ALTITUDE] [varchar](max) NULL,
	[TIMEZONE] [varchar](max) NULL,
	[DST] [varchar](max) NULL,
	[TZ_DATABASE_TIMEZONE] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Calendar]    Script Date: 11/22/2017 2:21:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calendar](
	[date_full] [datetime] NULL,
	[month_num] [float] NULL,
	[week_day] [float] NULL,
	[week_day_full] [nvarchar](255) NULL,
	[workingday] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NEW_Arrivals1]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NEW_Arrivals1](
	[ID] [nvarchar](255) NULL,
	[Origin] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Weekdays] [nvarchar](255) NULL,
	[Departure] [time](7) NULL,
	[Arrival] [time](7) NULL,
	[Flight No] [nvarchar](255) NULL,
	[Period of operation] [nvarchar](255) NULL,
	[ValidFrom] [nvarchar](255) NULL,
	[ValidTo] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL,
	[PRICE] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_calendar]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_calendar](
	[date_full] [date] NOT NULL,
	[month_num] [int] NOT NULL,
	[week_day] [int] NOT NULL,
	[week_day_full] [varchar](50) NOT NULL,
	[workingday] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[testLH]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[testLH](
	[ID] [nvarchar](255) NULL,
	[Origin] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Weekdays] [nvarchar](255) NULL,
	[Departure] [nvarchar](255) NULL,
	[Arrival] [nvarchar](255) NULL,
	[Flight No] [nvarchar](255) NULL,
	[Period of operation] [nvarchar](255) NULL,
	[ValidFrom] [nvarchar](255) NULL,
	[ValidTo] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL,
	[DIFF] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_Departures]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_Departures](
	[ID] [nvarchar](255) NULL,
	[Origin] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Weekdays] [nvarchar](255) NULL,
	[Departure] [time](7) NULL,
	[Arrival] [time](7) NULL,
	[Flight No] [nvarchar](255) NULL,
	[Period of operation] [nvarchar](255) NULL,
	[ValidFrom] [nvarchar](255) NULL,
	[ValidTo] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_DEPARTURES2]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_DEPARTURES2](
	[ID] [nvarchar](255) NULL,
	[ORIGIN] [nvarchar](255) NULL,
	[DESTINATION] [nvarchar](255) NULL,
	[WEEKDAYS] [nvarchar](255) NULL,
	[DEPARTURE] [time](7) NULL,
	[ARRIVAL] [time](7) NULL,
	[FLIGHT NO] [nvarchar](255) NULL,
	[PERIOD OF OPERATION] [nvarchar](255) NULL,
	[VALIDFROM] [nvarchar](255) NULL,
	[VALIDTO] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_DEPARTURES3]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_DEPARTURES3](
	[ID] [nvarchar](255) NULL,
	[ORIGIN] [nvarchar](255) NULL,
	[DESTINATION] [nvarchar](255) NULL,
	[WEEKDAYS] [nvarchar](255) NULL,
	[DEPARTURE] [time](7) NULL,
	[ARRIVAL] [time](7) NULL,
	[FLIGHT NO] [nvarchar](255) NULL,
	[PERIOD OF OPERATION] [nvarchar](255) NULL,
	[VALIDFROM] [nvarchar](255) NULL,
	[VALIDTO] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL,
	[Diff] [numeric](23, 10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_DEPARTURES4]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_DEPARTURES4](
	[ID] [nvarchar](255) NULL,
	[ORIGIN] [nvarchar](255) NULL,
	[DESTINATION] [nvarchar](255) NULL,
	[WEEKDAYS] [nvarchar](255) NULL,
	[DEPARTURE] [time](7) NULL,
	[ARRIVAL] [time](7) NULL,
	[FLIGHT NO] [nvarchar](255) NULL,
	[PERIOD OF OPERATION] [nvarchar](255) NULL,
	[VALIDFROM] [nvarchar](255) NULL,
	[VALIDTO] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL,
	[Diff] [numeric](23, 10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_FINAL_TABLE]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_FINAL_TABLE](
	[TYPE] [varchar](1) NOT NULL,
	[ID] [nvarchar](255) NULL,
	[Origin] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Weekdays] [nvarchar](255) NULL,
	[Departure] [time](7) NULL,
	[Arrival] [time](7) NULL,
	[Flight No] [nvarchar](255) NULL,
	[Period of operation] [nvarchar](255) NULL,
	[ValidFrom] [nvarchar](255) NULL,
	[ValidTo] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL,
	[DIFF] [numeric](23, 10) NULL,
	[price] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_Flights]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_Flights](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Origin] [varchar](10) NOT NULL,
	[Destination] [varchar](10) NOT NULL,
	[Weekdays] [varchar](50) NOT NULL,
	[Departure] [varchar](20) NOT NULL,
	[Arrival] [varchar](20) NOT NULL,
	[Flight No] [varchar](20) NOT NULL,
	[Period of operation] [varchar](50) NULL,
	[ValidFrom] [varchar](20) NOT NULL,
	[ValidTo] [varchar](20) NOT NULL,
	[AL_IATA] [varchar](10) NOT NULL,
	[InsertDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_Flights_Arrivals]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_Flights_Arrivals](
	[ID] [nvarchar](255) NULL,
	[Origin] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Weekdays] [nvarchar](255) NULL,
	[Departure] [nvarchar](255) NULL,
	[Arrival] [datetime] NULL,
	[Flight No] [nvarchar](255) NULL,
	[Period of operation] [nvarchar](255) NULL,
	[ValidFrom] [nvarchar](255) NULL,
	[ValidTo] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIA_Flights_Departures]    Script Date: 11/22/2017 2:21:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIA_Flights_Departures](
	[ID] [nvarchar](255) NULL,
	[Origin] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Weekdays] [nvarchar](255) NULL,
	[Departure] [datetime] NULL,
	[Arrival] [datetime] NULL,
	[Flight No] [nvarchar](255) NULL,
	[Period of operation] [nvarchar](255) NULL,
	[ValidFrom] [nvarchar](255) NULL,
	[ValidTo] [nvarchar](255) NULL,
	[AL_IATA] [nvarchar](255) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Airlines] ADD  DEFAULT (getdate()) FOR [InsertDate]
GO
ALTER TABLE [dbo].[TIA_Flights] ADD  DEFAULT (getdate()) FOR [InsertDate]
GO
USE [master]
GO
ALTER DATABASE [Flights] SET  READ_WRITE 
GO
