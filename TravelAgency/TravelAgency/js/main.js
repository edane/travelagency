var way = "return";
var type = "LH";
$(document).ready(function () {
	
    $(".departing-calendar").datepicker();
    $(".returning-calendar").datepicker();
    $(".returning-time").timepicker({'timeFormat':'HH:mm'});
    $(".departing-time").timepicker({'timeFormat':'HH:mm'});
    $("#multistop-flight").hide(); 
    $(".dep").hide();
    $(".ret").hide();
    $("#continue").hide();

	var fromSuggestions = [];
	var toSuggestions = [];

	$.ajax({
		type: "POST",
		url: "Home.aspx/GetLocations",
		data: "",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: true,
		success: function (data, status) {
            var resultsArray = (data.d).split("+");
            var from_raw = JSON.parse(resultsArray[0]);
            var to_raw = JSON.parse(resultsArray[1]);

            for (var i = 0; i < from_raw.length; i++) {
                fromSuggestions.push(from_raw[i].Origin);
            }
            for (var i = 0; i < to_raw.length; i++) {
                toSuggestions.push(to_raw[i].Destination);
            }

            $(".from").autocomplete({
                source: fromSuggestions
            });
            $(".to").autocomplete({
                source: toSuggestions
            });

		},
		failure: function (data) {
			console.log(data.d);
		},
		error: function (data) {
			console.log(data.d);
		}
    });

    $("#sendMail").click(function () {
        var email = document.getElementById("usrMail").value;
        var password = document.getElementById("usrPsw").value;
        var name = document.getElementById("name").value;
        var origin1 = document.getElementById("origin1").innerHTML;
        var origin2 = "";
        var destination1 = document.getElementById("destination1").innerHTML;
        var destination2 = "";
        var price1 = document.getElementById("price1").innerHTML;
        var price2 = "";
        var date1 = document.getElementById("date1").innerHTML;
        var date2 = "";
        var time1 = document.getElementById("dep1").innerHTML;
        var time2 = "";

        if (way == "return") {
            origin2 = document.getElementById("origin2").innerHTML;
            destination2 = document.getElementById("destination2").innerHTML;
            price2 = document.getElementById("price2").innerHTML;
            date2 = document.getElementById("date2").innerHTML;
            time2 = document.getElementById("dep2").innerHTML;
        }

        $.ajax({
            type: "POST",
            url: "Home.aspx/SendEmail",
            data: "{'email': '" + email + "', 'password': '" + password + "', 'name':'" + name + "', 'origin1':'" + origin1 + "', 'origin2':'" + origin2 + "', 'destination1':'" + destination1 + "', 'destination2':'" + destination2 + "' , 'price1':'" + price1 + "', 'price2':'" + price2 + "', 'date1':'" + date1 + "', 'date2':'" + date2 + "', 'time1':'" + time1 + "', 'time2':'" + time2 + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (data, status) {
                $('#email-modal').modal('toggle');
                location.reload();
            },
            failure: function (data) {
                console.log(data.d);
            },
            error: function (data) {
                console.log(data.d);
            }
        });
    });

    $("#searchButton").click(function () {
        $("#continue").hide();
        flights = [{}, {}];
        
		var _origin = document.getElementById("from").value;
        var _arrival = document.getElementById("to").value;
        var dep_parts = document.getElementById("departing-calendar").value.trim().split("/");
        var dep_date = dep_parts[2] + "-" + dep_parts[0] + "-" + dep_parts[1];
        var dep_time = document.getElementById("departing-time").value.trim();

        if (way === "return") {
            var return_parts = document.getElementById("returning-calendar").value.trim().split("/");
            var return_date = return_parts[2] + "-" + return_parts[0] + "-" + return_parts[1];
            var return_time = document.getElementById("returning-time").value.trim();

            $.ajax({
                type: "POST",
                url: "Home.aspx/GetDepartingsReturns",
                data: "{'origin': '" + _origin + "', 'destination': '" + _arrival + "', 'dep_date': '" + dep_date + "', 'dep_time': '" + dep_time + "', 'return_date': '" + return_date + "', 'return_time': '" + return_time  + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (data, status) {
                    var resultsArray = (data.d).split("+");
                    var dest = JSON.parse(resultsArray[0]);
                    var returns = JSON.parse(resultsArray[1]);
                    if (dest.length === 0 || returns.length === 0) {
                        document.getElementById("departures").innerHTML = "";
                        document.getElementById("returns").innerHTML = "";
                        document.getElementById("departures").innerHTML +=
                            '<div class="row flights-container">'
                            + '<div class="col-1"></div>'
                            + '<div class="results-element row col-10"> No results for your search </div>';
                        $(".dep").show();
                        $("#departure-labels").hide();
                        $("#return-labels").hide();
                    } else {
                        document.getElementById("departures").innerHTML = "";
                        document.getElementById("returns").innerHTML = "";
                        $("#departure-labels").show();
                        $("#return-labels").show();
                        $(".dep").hide();
                        showDepartures(dest);
                        showReturns(returns);
                    }
                },
                failure: function (data) {
                    console.log(data.d);
                },
                error: function (data) {
                    console.log(data.d);
                }
            });

        } else if (way === "oneway") {
            $.ajax({
                type: "POST",
                url: "Home.aspx/GetOneWay",
                data: "{'origin': '" + _origin + "', 'destination': '" + _arrival + "', 'date': '" + dep_date + "', 'time': '" + dep_time+ "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (data, status) {
                    var result = JSON.parse(data.d);
                    if (result.length === 0) {
                        document.getElementById("departures").innerHTML = "";
                        document.getElementById("returns").innerHTML = "";
                        document.getElementById("departures").innerHTML +=
                            '<div class="row flights-container">'
                            + '<div class="col-1"></div>'
                            + '<div class="results-element row col-10"> No results for your search </div>';
                        $(".dep").show();
                        $("#departure-labels").hide();
                    } else {
                        document.getElementById("departures").innerHTML = "";
                        $("#departure-labels").show();
                        $(".dep").hide();
                            showDepartures(result);
                    }

                },
                failure: function (data) {
                    console.log(data.d);
                },
                error: function (data) {
                    console.log(data.d);
                }
            });
        }
		
    });

    function showDepartures(data) {
        console.log(data)

        for (var i = 0; i < data.length; i++) {
            $("#dep_flight_btn-" + i).removeClass("selected-radio").addClass("unselected-radio");
            document.getElementById("dep-from").innerHTML = '<div class="col-9">' + data[0].ORIGIN + " to <b>" + data[0].DESTINATION + "</b></div>";
            var std_hour = data[i].STD.Hours < 10 ? '0' + data[i].STD.Hours : data[i].STD.Hours;
            var std_min = data[i].STD.Minutes < 10 ? '0' + data[i].STD.Minutes : data[i].STD.Minutes;
            var sta_hour = data[i].STA.Hours < 10 ? '0' + data[i].STA.Hours : data[i].STA.Hours;
            var sta_min = data[i].STA.Minutes < 10 ? '0' + data[i].STA.Minutes : data[i].STA.Minutes;
            var date_parts = data[i].FLTDATE.split("-");
            document.getElementById("dep-from").innerHTML += '<div class="col-8 "> on ' + date_parts[1] + '/' + date_parts[2] + '/' + date_parts[0] + ' </div >';
            var funcd = "selectFlight(this.id, 'dep_flight', " + i + "," + data.length + ")";

            var minutes = Math.round(data[i].BLOCK * 60)
            var duration_h = Math.floor(minutes / 60) < 10 ? '0' + Math.floor(minutes / 60) : Math.floor(minutes / 60);
            var duration_m = minutes % 60 < 10 ? '0' + minutes % 60 : minutes % 60;
            var duration = duration_h + ":" + duration_m;
            var img = '';
            var radioButton = '<div class="col-12 unselected" origin="' + data[i].ORIGIN + '" destination="' + data[i].DESTINATION + '" price="' + data[i].TOTAL_PRICE + '" std="' + std_hour + ':' + std_min
                + '" sta="' + sta_hour + ':' + sta_min + '" date="' + date_parts[1] + '/' + date_parts[2] + '/' + date_parts[0] + '" id="dep_flight_btn-' + i + '" onClick="' + funcd + '"></div>';

            if (typeof (data[i].AL_IATA) !== 'undefined') {
                img = '<img class="company-logo" src="../img/' + data[i].AL_IATA + '.png">'
            } else {
                img = '<img class="company-logo" src="../img/' + data[i].AL + '.png">'
            }

            document.getElementById("departures").innerHTML +=
                '<div class="row flights-container" id="dep_flight-' + i + '">'
                + ' <div class="col-1"></div>'
                + '<div class="results-element row col-10">'
                + ' <div class="col-10 ">'
                + ' <div class="row logo-container">'
                + '<div class="col-5">' + img + '</div>'
                + '<div class="col-5 small-header-text " >Duration: ' + duration + ' </div >'
                + '</div>'
                + '<div class="col-12">'
                + '<div class="row align-center">'
                + '<div class="col-2"></div>'
                + '<div class="col-2">' + std_hour + ':' + std_min + '</div>'
                + '<div class="col-2 line"></div>'
                + '<div class="col-1 plane"><span class="glyphicon glyphicon-plane rotate"></span></div>'
                + '<div class="col-2 line"></div>'
                + '<div class="col-2">' + sta_hour + ':' + sta_min + '</div>'
                + '</div>'
                + '<div class="row align-center">'
                + '<div class="col-2"></div>'
                + '<div class="col-2">' + data[i].ORIGIN + '</div>'
                + '<div class="col-5 small-text"></div>'
                + '<div class="col-2">' + data[i].DESTINATION + '</div>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<div class="col-2 send-mail">'
                + '<div class="col-12" style= " margin-top: 10px;">&euro;' + data[i].TOTAL_PRICE + '</div>' + radioButton
                + '</div>'
                + '</div>'
                + '</div>';
        }
        $(".dep").show();
    }
    
    function showReturns(data) {
        for (var i = 0; i < data.length; i++) {
            $("#ret_flight_btn-" + i).removeClass("selected-radio").addClass("unselected-radio");
            document.getElementById("returns-from").innerHTML = '<div class="col-9">'+data[0].ORIGIN + " to <b>" + data[0].DESTINATION + "</b></div>";
            var std_hour = data[i].STD.Hours < 10 ? '0' + data[i].STD.Hours : data[i].STD.Hours;
            var std_min = data[i].STD.Minutes < 10 ? '0' + data[i].STD.Minutes : data[i].STD.Minutes;
            var sta_hour = data[i].STA.Hours < 10 ? '0' + data[i].STA.Hours : data[i].STA.Hours;
            var sta_min = data[i].STA.Minutes < 10 ? '0' + data[i].STA.Minutes : data[i].STA.Minutes;
            var date_parts = data[i].FLTDATE.split("-");
            document.getElementById("returns-from").innerHTML += '<div class="col-8 "> on ' + date_parts[1] + '/' + date_parts[2] + '/' + date_parts[0] + ' </div >';
            var funcr = "selectFlight(this.id, 'ret_flight', " + i + "," + data.length + ")";
            var minutes = Math.round(data[i].BLOCK * 60)
            var duration_h = Math.floor(minutes / 60) < 10 ? '0' + Math.floor(minutes / 60) : Math.floor(minutes / 60);
            var duration_m = minutes % 60 < 10 ? '0' + minutes % 60 : minutes % 60 ;
            var duration = duration_h + ":" + duration_m;
            var img = '';
            var radioButton = '<div class="col-12 unselected" origin="' + data[i].ORIGIN + '" destination="' + data[i].DESTINATION + '" price="' + data[i].TOTAL_PRICE + '" std="' + std_hour + ':' + std_min
                            + '" sta="' + sta_hour + ':' + sta_min + '" date="' + date_parts[1] + '/' + date_parts[2] + '/' + date_parts[0] + '" id="ret_flight_btn-' + i + '" onClick="' + funcr + '"></div>';


            if (typeof (data[i].AL_IATA) !== 'undefined') {
                img = '<img class="company-logo" src="../img/' + data[i].AL_IATA + '.png">'
            } else {
                img = '<img class="company-logo" src="../img/' + data[i].AL + '.png">'
            }

            document.getElementById("returns").innerHTML +=
                '<div class="row flights-container" id="ret_flight-' + i + '">'
                + '<div class="col-1"></div>'
                + '<div class="results-element row col-10">'
                + '<div class="col-10 ">'
                + '<div class="row logo-container">'
                + '<div class="col-5">'+img+'</div>'
                + '<div class="col-5 small-header-text " >Duration: ' + duration + '  </div >'
                + '</div>'
                + '<div class="col-12">'
                + '<div class="row align-center">'
                + '<div class="col-2"></div>'
                + '<div class="col-2">' + std_hour + ':' + std_min + '</div>'
                + '<div class="col-2 line"></div>'
                + '<div class="col-1 plane"><span class="glyphicon glyphicon-plane rotate"></span></div>'
                + '<div class="col-2 line"></div>'
                + '<div class="col-2">' + sta_hour + ':' + sta_min + '</div>'
                + '</div>'
                + '<div class="row align-center">'
                + '<div class="col-2"></div>'
                + '<div class="col-2">' + data[i].ORIGIN + '</div>'
                + '<div class="col-5 small-text"></div>'
                + '<div class="col-2">' + data[i].DESTINATION + '</div>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<div class="col-2 send-mail">'
                + '<div class="col-12" style= " margin-top: 10px;">&euro;' + data[i].TOTAL_PRICE + '</div>' + radioButton
                + '</div>'
                + '</div>'
                + '</div>';
        }
        $(".ret").show();
    }

    


    $("#return-radio").click(function(){
        if($(this).is(".unselected-radio")){
            $(this).removeClass("unselected-radio").addClass("selected-radio");
            $("#oneway-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#multistop-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#weekly-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#standard-flight").show();
            $("#multistop-flight").hide();
            $(".returning-calendar").prop('disabled', false);
            $(".calendar").prop('disabled', false);
            $(".returning-time").prop('disabled', false);
			$(".time").prop('disabled', false);
			$(".returning-calendar").css("background-color", "#ffffff");
            $(".returning-time").css("background-color", "#ffffff");
            $(".transfer-arrows").show();
            way = "return";
            $(".dep").hide();
            $(".ret").hide();
            $("#continue").hide();
        }
    });

    $("#oneway-radio").click(function(){
        if($(this).is(".unselected-radio")){
            $(this).removeClass("unselected-radio").addClass("selected-radio");
            $("#return-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#multistop-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#weekly-radio").removeClass("selected-radio").addClass("unselected-radio");
            $(".returning-calendar").prop('disabled', true);
            $(".calendar").prop('disabled', true);
            $(".returning-time").prop('disabled', true);
            $(".time").prop('disabled', true);
            $(".returning-calendar").css("background-color", "#cccccc");
            $(".returning-time").css("background-color", "#cccccc");
            $("#standard-flight").show(); 
            $("#multistop-flight").hide();
            $(".transfer-arrows").hide();
            way = "oneway";
            $(".dep").hide();
            $(".ret").hide();
            $("#continue").hide();
        }
    });

    $("#multistop-radio").click(function(){
        if($(this).is(".unselected-radio")){
            $(this).removeClass("unselected-radio").addClass("selected-radio");
            $("#oneway-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#return-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#weekly-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#standard-flight").hide();
            $("#multistop-flight").show();
            way = "multistop";
            $(".dep").hide();
            $(".ret").hide();
            $("#continue").hide();
        }
    });
    $("#weekly-radio").click(function(){
        if($(this).is(".unselected-radio")){
            $(this).removeClass("unselected-radio").addClass("selected-radio");
            $("#oneway-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#multistop-radio").removeClass("selected-radio").addClass("unselected-radio");
            $("#return-radio").removeClass("selected-radio").addClass("unselected-radio");
            $(".dep").hide();
            $(".ret").hide();
            $("#continue").hide();
        }
    });
    $("#lufthansa-radio").click(function(){
        if($(this).is(".unselected-radio")){
            $(this).removeClass("unselected-radio").addClass("selected-radio");
            $("#allairlines-radio").removeClass("selected-radio").addClass("unselected-radio");
            type = "LH"
        }
    });
    $("#allairlines-radio").click(function(){
        if($(this).is(".unselected-radio")){
            $(this).removeClass("unselected-radio").addClass("selected-radio");
            $("#lufthansa-radio").removeClass("selected-radio").addClass("unselected-radio");
            type = "all";
        }
    });

    $(".departing-calendar").click(function() {
        $(this).datepicker('show');
    })

    $(".returning-calendar").click(function() {
        $(this).datepicker('show');
    });

    $(".add-flight-button").click(function() {
        document.getElementById("field-container").innerHTML += '<div class="row"><div class="col-3 radio-container"><input type="text" placeholder="From" class="from input-field"><span class="glyphicon glyphicon-globe big-icon-align"></span>'
        +' </div> <div class="col-3 radio-container"><input type="text" placeholder="To" class="to input-field"><span class="glyphicon glyphicon-globe big-icon-align"></span></div>'
        +' <div class="col-3 radio-container"><input type="text" placeholder="Departing" class="departing-calendar input-field"><span class="glyphicon glyphicon-calendar aligned-icon"></span></div>'
        +' <div class="col-2 radio-container"> <input type="text" placeholder="00:00" class="departing-time input-field"><span class="glyphicon glyphicon-chevron-down aligned-icon"></span></div></div><br>'
    });

    $(".cal1").click(function () {
        $("#departing-calendar").datepicker('show');
    });

    $(".cal2").click(function () {
        $("#returning-calendar").datepicker('show');
    });

    $(".arr1").click(function () {
        $("#departing-time").timepicker({ 'timeFormat': 'HH:mm' });
    });

    $(".arr2").click(function () {
        $("#returning-time").timepicker({ 'timeFormat': 'HH:mm' });
    });

    $(".transfer-arrows").click(function () {
        var from = document.getElementById("from").value;
        document.getElementById("from").value = document.getElementById("to").value;
        document.getElementById("to").value = from;
    });

    $("#cancel").click(function () {
        $('#email-modal').modal('toggle');
    });

    $("#continue").click(function () {

        $("#firstflight").hide();
        $("#secondflight").hide();

        $('#email-modal').modal();
        if (flights[0].length !== 0) {
            document.getElementById("date1").innerHTML = flights[0].date;
            document.getElementById("dep1").innerHTML = flights[0].std;
            document.getElementById("arrival1").innerHTML = flights[0].sta;
            document.getElementById("origin1").innerHTML = flights[0].origin;
            document.getElementById("destination1").innerHTML = flights[0].destination;
            document.getElementById("price1").innerHTML = "&euro;" + flights[0].price;
            $("#firstflight").show();
        }


        if (way == "return" && flights[1].length !== 0) {
            document.getElementById("date2").innerHTML = flights[1].date;
            document.getElementById("dep2").innerHTML = flights[1].std;
            document.getElementById("arrival2").innerHTML = flights[1].sta;
            document.getElementById("origin2").innerHTML = flights[1].origin;
            document.getElementById("destination2").innerHTML = flights[1].destination;
            document.getElementById("price2").innerHTML = "&euro;" + flights[1].price;
            $("#secondflight").show();
        }
    })
    

});

var flights = [{}, {}];
function selectFlight(btn_id, id, index, length) {
       
    for (var i = 0; i < length; i++) {
        if (i !== index) {

            $("#" + id + "_btn-" + i).removeClass("selected").addClass("unselected");
        } else {
            $("#" + id + "_btn-" + i).removeClass("unselected").addClass("selected");
            if (id === "dep_flight") {
                flights[0].origin = document.getElementById(btn_id).getAttribute("origin");
                flights[0].destination = document.getElementById(btn_id).getAttribute("destination");
                flights[0].date = document.getElementById(btn_id).getAttribute("date");
                flights[0].std = document.getElementById(btn_id).getAttribute("std");
                flights[0].sta = document.getElementById(btn_id).getAttribute("sta");
                flights[0].price = document.getElementById(btn_id).getAttribute("price");
            }
            else if (id === "ret_flight") {
                flights[1].origin = document.getElementById(btn_id).getAttribute("origin");
                flights[1].destination = document.getElementById(btn_id).getAttribute("destination");
                flights[1].date = document.getElementById(btn_id).getAttribute("date");
                flights[1].std = document.getElementById(btn_id).getAttribute("std");
                flights[1].sta = document.getElementById(btn_id).getAttribute("sta");
                flights[1].price = document.getElementById(btn_id).getAttribute("price");
            }
            if (way == "oneway" && flights[0].hasOwnProperty("origin")) {
                $("#continue").show();
            }
            if (way == "return" && flights[0].hasOwnProperty("origin") && flights[1].hasOwnProperty("origin")) {
                $("#continue").show();
            }
                
        }
    }

        
}
    
    