﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="TravelAgency.Home" %>

<!DOCTYPE html>

<html>
<head>
	<link rel="stylesheet" href="css/custom.css">
	<link rel="stylesheet" href="css/jquery.timepicker.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="js/main.js"></script>
	<script src="js/jquery.timepicker.min.js"></script>
	
</head>
<body>
	<div class="logo"></div>
	<div class="custom-container-search container">
        <form  name="myform">
		<div class="row head-row">
			<div class="col-12 head-element element-selected">
			<span class="glyphicon glyphicon-plane rotate"></span>
				Flights
			</div>
		</div>
		<div class="row radio-button-row">
            <div class="col-1"></div>
			<div class="col-3 radio-container">
				<div id="return-radio" class="selected-radio col-1"></div>
				<div class="radio-label">Return</div>
			</div>
			<div class="col-3 radio-container">
				<div id="oneway-radio" class="unselected-radio col-1"></div>
				<div class="radio-label">One way</div>
			</div>
			<!--<div class="col-3 radio-container">
				<div id="multistop-radio" class="unselected-radio col-1"></div>
				<div class="radio-label">Multi-Stop flight</div>
			</div>
			<div class="col-3 radio-container">
				<div id="weekly-radio" class="unselected-radio col-1"></div>
				<div class="radio-label">Weekly overview</div>
			</div>-->
		</div>
		<div id="standard-flight">
			<div class="row">
				<div class="col-5 radio-container">
					<input type="text" placeholder="From" id="from" class="from input-field" required/>
					<span class="glyphicon glyphicon-globe big-icon-align"></span>
				</div>
				<div class="col-1">
					<span class="glyphicon glyphicon-transfer transfer-arrows"></span>
				</div>
				<div class="col-5 radio-container">
					<input type="text" placeholder="To" id="to" class="to input-field" required/>
					<span class="glyphicon glyphicon-globe big-icon-align"></span>
				</div>
			</div>
			<div class="row custom-row">
				<div class="col-3 radio-container">
					<input type="text" id="departing-calendar" placeholder="Departing" class="departing-calendar input-field" required/>
					<span class="glyphicon glyphicon-calendar cal1 aligned-icon"></span>
				</div>
				<div class="col-2 radio-container">
					<input type="text" id="departing-time" placeholder="00:00" class="departing-time input-field">
					<span class="glyphicon glyphicon-chevron-down arr1 aligned-icon"></span>
				</div>
				<div class="col-1"></div>
				<div class="col-3 radio-container">
					<input type="text" id="returning-calendar" placeholder="Returning" class="returning-calendar input-field" required/>
					<span id="calendar" class="glyphicon glyphicon-calendar cal2 aligned-icon"></span>
				</div>
				<div class="col-2 radio-container">
					<input type="text" id="returning-time" placeholder="00:00" class="returning-time input-field">
					<span id="time" class="glyphicon glyphicon-chevron-down arr2 aligned-icon"></span>
				</div>
			</div>
		</div>
		<div id="multistop-flight">
			<div id="field-container">
			<div class="row">
				<div class="col-3 radio-container">
					<input type="text" placeholder="From" class="from input-field">
					<span class="glyphicon glyphicon-globe big-icon-align"></span>
				</div>
				<div class="col-3 radio-container">
					<input type="text" placeholder="To" class="to input-field" >
					<span class="glyphicon glyphicon-globe big-icon-align"></span>
				</div>
				<div class="col-3 radio-container">
					<input type="text" placeholder="Departing" class="departing-calendar input-field">
					<span class="glyphicon glyphicon-calendar aligned-icon"></span>
				</div>
				<div class="col-2 radio-container">
					<input type="text" placeholder="00:00" class="departing-time input-field">
					<span class="glyphicon glyphicon-chevron-down aligned-icon"></span>
				</div>
			</div><br>
			<div class="row">
				<div class="col-3 radio-container">
					<input type="text" placeholder="From" class="from input-field" required>
					<span class="glyphicon glyphicon-globe big-icon-align"></span>
				</div>
				<div class="col-3 radio-container">
					<input type="text" placeholder="To" class="to input-field" required>
					<span class="glyphicon glyphicon-globe big-icon-align"></span>
				</div>
				<div class="col-3 radio-container">
					<input type="text" placeholder="Departing" class="departing-calendar input-field">
					<span class="glyphicon glyphicon-calendar aligned-icon"></span>
				</div>
				<div class="col-2 radio-container">
					<input type="text" placeholder="00:00" class="departing-time input-field">
					<span class="glyphicon glyphicon-chevron-down aligned-icon"></span>
				</div>
			</div><br>
			</div>
			<div class="row">
				<div class="col-10"></div>
				<input type="button" class="add-flight-button col-2" value="Add flight">
			</div>
		</div>
		<div class="row custom-row">
			<div class="col-1">
                <!--<select id="sort" class="input-field">
                  <option value="std">Departure Time</option>
                  <option value="sta">Arrival Time</option>
                </select>-->
			</div>
			<!--<div class="col-3 radio-container">
				<div id="lufthansa-radio" class="selected-radio col-1"></div>
				<div class="radio-label">Lufthansa</div>
			</div>
			<div class="col-1"></div>
			<div class="col-3 radio-container">
				<div id="allairlines-radio" class="unselected-radio col-1"></div>
				<div class="radio-label">All airlines</div>
			</div>-->
			<input type="submit" id="searchButton" class="search-button col-9" value="Search flights">
		</div>

        </form>
	</div>
	<div class="custom-container-results dep container">
		<div class="row" id="departure-labels">
			<span class="glyphicon glyphicon-plane rotate big-plane-icon"></span>
			<div class="big-white-text row" id="dep-from">
			</div>
		</div>
        <div id="departures">
        </div>
	</div>
    <div class="custom-container-results ret container">
		<div class="row" id="return-labels">
			<span class="glyphicon glyphicon-plane rotate big-plane-icon"></span>
			<div class="big-white-text row"  id="returns-from">
            </div>
		</div>
		<div id="returns">
        </div>
        
	</div>
    <div class="row containers">
        <div class="col-2"></div>
        <div class="col-8 bottom-button"><input type="button" id="continue" class="email-button col-12" value="CONTINUE"></div>
    </div>
    <div class="modal fade modal-result" id="email-modal" role="dialog" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-1"></div>
                    <h1 class="col-10">Flight Booking</h1>
                </div>
                <div class="row flights-container" id="firstflight">
                    <div class="col-1"></div>
			        <div class="results-element row col-11" style="background-color: #ccc">
				        <div class="col-12 ">
					        <div class="row logo-container">
                                <div class="col-6" id="date1"></div>
                                <div class="col-6" id="price1" style="text-align: right;"></div>
					        </div>
					        <div class="col-12">
						        <div class="row align-center">
							        <div class="col-2" id="dep1"></div>
                                    <div class="col-1"></div>
							        <div class="col-2 line"></div>
							        <div class="col-1 plane"><span class="glyphicon glyphicon-plane rotate"></span></div>
							        <div class="col-2 line"></div>
							        <div class="col-2" id="arrival1"></div>
						        </div>
						        <div class="row align-center">
							        <div class="col-2"  id="origin1"> </div>
                                    <div class="col-1"></div>
							        <div class="col-5 small-text"> <div id="airline1"></div></div>
							        <div class="col-2" id="destination1"></div>
						        </div>
					        </div>
				        </div>
			        </div>
		        </div>
                <div class="row flights-container" id="secondflight">
                    <div class="col-1"></div>
			        <div class="results-element row col-11" style="background-color: #ccc">
				        <div class="col-12 ">
					        <div class="row logo-container">
                                <div class="col-6"  id="date2"></div>
                                <div class="col-6" id="price2" style="text-align: right;"></div>
					        </div>
					        <div class="col-12">
						        <div class="row align-center">
							        <div class="col-2" id="dep2"></div>
                                    <div class="col-1"></div>
							        <div class="col-2 line"></div>
							        <div class="col-1 plane"><span class="glyphicon glyphicon-plane rotate"></span></div>
							        <div class="col-2 line"></div>
							        <div class="col-2" id="arrival2"></div>
						        </div>
						        <div class="row align-center">
							        <div class="col-2" id="origin2"></div>
                                    <div class="col-1"></div>
							        <div class="col-5 small-text">Operated by <div id="airline2"></div></div>
							        <div class="col-2" id="destination2"></div>
						        </div>
					        </div>
				        </div>
			        </div>
                    </div>
		        </div>
                <div class="row modal-custom-row">
                    <div class="col-6">Your E-mail:* </div>
                    <input type="text" id="usrMail" placeholder="Email" class="input-field col-11" required>
                </div>
                <div class="row modal-custom-row">
                    <div class="col-6">Your E-mail password:* </div>
                    <input type="password" id="usrPsw" placeholder="Password" class="input-field col-11" required>
                </div>
                <div class="row modal-custom-row">
                    <div class="col-6">Full Name:* </div>
                    <input type="text" id="name" placeholder="Full Name" class="input-field col-11" required>
                </div>
                <div class="row"></div>
                <div class="row modal-custom-row">
                    <div class="col-1"></div>
                    <button type="button" id="sendMail" class="col-4 btn btn-primary btn-lg btn-block">Send E-mail</button>
                    <div class="col-1"></div>
                    <button type="button" id="cancel" class="col-4 btn btn-primary btn-lg btn-block">Cancel</button>
                </div>
            </div>
          </div>
         </div>
    </div>
</body>
</html>    
