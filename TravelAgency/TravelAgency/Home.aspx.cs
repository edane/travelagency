﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TravelAgency
{
    public partial class Home : System.Web.UI.Page
    {
        static SqlConnection conn = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=Flights;Integrated Security=True;MultipleActiveResultSets=true");
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

        }

        [WebMethod]
        [ScriptMethod]
        public static void SendEmail(string email, string password, string name, string origin1, string origin2, string destination1, string destination2, 
            string price1, string price2, string date1, string date2, string time1, string time2)
        {
            var fromAddress = new MailAddress(email);
            var fromPassword = password;
            var toAddress = new MailAddress("lenastf6@gmail.com");

            string subject = "Flight Booking";
            string body = "<p>Dear Sir/Madam,</p>"
                + "<p>Could you please book the following flight/flights for Mr./Mrs. "+name+"?</p>"
                + "<table style = 'width: 572px;' cellspacing = '10' cellpadding = '10'>"
                + "<tbody>"
                + "<tr>"
                + "<td style = 'width: 266px;' >"
                + "<p><strong> Outbound Flight Information</strong></ p >"
                + "<p><em> From: </em>" + origin1 + "</p>"
                + "<p><em> To: </em>" + destination1 + "</p>"
                + "<p><em> Date: </em>" + date1 + "</p>"
                + "<p><em> Time: </em>" + time1 + "</p>"
                + "<p><em> Price: </em>" + price1 + "</p>"
                + "</td>";
            if(origin2 != "" && destination2 != "")
            {
                body = body +
                    "<td style = 'width: 308px;' >"
                    + "<p><strong> Return Flight Information</strong></p>"
                    + "<p><em> From: </em>" + origin2 + "</p>"
                    + "<p><em> To:</em>" + destination2 + " </p>"
                    + "<p><em> Date: </em>" + date1 + "</p>"
                    + "<p><em> Time:</em>" + time2 + "</p>"
                    + "<p><em> Price:</em>" + price2 + "</p>"
                    + "</td>";
            }
            body = body 
                +"</tr>"
                + "</tbody>"
                +"</table>"
                + "<p>Best regards<br /></p>";

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

            };

            var message = new MailMessage(fromAddress, toAddress);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;

            smtp.Send(message);
        }

        [WebMethod]
        [ScriptMethod]
        public static Object GetLocations()
        {
            string fromQuery = "SELECT  distinct [Origin] FROM[Flights].[dbo].[Flights_tbl]";
            string toQuery = "SELECT  distinct [Destination] FROM[Flights].[dbo].[Flights_tbl]";

            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
                
            }
            SqlCommand cmdTo = new SqlCommand(toQuery, conn);
            SqlCommand cmdFrom = new SqlCommand(fromQuery, conn);

            var details1 = new List<Dictionary<string, object>>();
            var details2 = new List<Dictionary<string, object>>();

            using (SqlDataReader rdr = cmdFrom.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var dict = new Dictionary<string, object>();

                        for (int i = 0; i < rdr.FieldCount; i++)
                        {
                            dict.Add(rdr.GetName(i), rdr.IsDBNull(i) ? null : rdr.GetValue(i));
                        }

                        details1.Add(dict);
                    }
                }
            }

            using (SqlDataReader rdr = cmdTo.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var dict = new Dictionary<string, object>();

                        for (int i = 0; i < rdr.FieldCount; i++)
                        {
                            dict.Add(rdr.GetName(i), rdr.IsDBNull(i) ? null : rdr.GetValue(i));
                        }

                        details2.Add(dict);
                    }
                }
            }

            JavaScriptSerializer jss = new JavaScriptSerializer();
            string jsonDoc1 = jss.Serialize(details1);
            string jsonDoc2 = jss.Serialize(details2);

            string jsonDoc = jsonDoc1 + " + " + jsonDoc2;

            return jsonDoc;
        }
        

        [WebMethod]
        [ScriptMethod]
        public static Object GetDepartingsReturns(string origin, string destination, string dep_date, string dep_time, string return_date, string return_time)
        {
            string departuresQuery = "";
            string returnsQuery = "";
            
            departuresQuery = "SELECT TOP (10) [AL],[ORIGIN],[DESTINATION1],cast([FLTDATE] as varchar(20)) [FLTDATE] "
                                + ",[STD],[STA],[BLOCK],[FNR],[PRICE1],[SPLIT]"
                                + ",[TRANZ],[ORIGIN2],[DESTINATION],[WEEKDAYS] "
                                + ",[VALIDFROM],[VALIDTO],[DEPARTURE],[ARRIVAL],[DIFF],[AL_IATA]"
                                + ",[FLIGHT NO],[PRICE2],[TOTAL_TIME],[TOTAL_PRICE] "
                                + "FROM ONE_STOP('" + origin + "','" + destination + "','" + dep_date + "','" + dep_time + "') where [TOTAL_PRICE] is not null  order by [TOTAL_TIME] asc";
            returnsQuery = "SELECT TOP (10) [AL],[ORIGIN],[DESTINATION1],cast([FLTDATE] as varchar(20)) [FLTDATE] "
                                + ",[STD],[STA],[BLOCK],[FNR],[PRICE1],[SPLIT]"
                                + ",[TRANZ],[ORIGIN2],[DESTINATION],[WEEKDAYS] "
                                + ",[VALIDFROM],[VALIDTO],[DEPARTURE],[ARRIVAL],[DIFF],[AL_IATA]"
                                + ",[FLIGHT NO],[PRICE2],[TOTAL_TIME],[TOTAL_PRICE] "
                                + "FROM ONE_STOP('" + destination + "','" + origin + "','" + return_date + "','" + return_time + "') where [TOTAL_PRICE] is not null order by [TOTAL_TIME] asc";


            SqlCommand cmdDepartures = new SqlCommand(departuresQuery, conn);
            SqlCommand cmdReturns = new SqlCommand(returnsQuery, conn);

            var details1 = new List<Dictionary<string, object>>();
            var details2 = new List<Dictionary<string, object>>();

            using (SqlDataReader rdr = cmdDepartures.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var dict = new Dictionary<string, object>();

                        for (int i = 0; i < rdr.FieldCount; i++)
                        {
                            dict.Add(rdr.GetName(i), rdr.IsDBNull(i) ? null : rdr.GetValue(i));
                        }

                        details1.Add(dict);
                    }
                }
            }

            using (SqlDataReader rdr = cmdReturns.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var dict = new Dictionary<string, object>();

                        for (int i = 0; i < rdr.FieldCount; i++)
                        {
                            dict.Add(rdr.GetName(i), rdr.IsDBNull(i) ? null : rdr.GetValue(i));
                        }

                        details2.Add(dict);
                    }
                }
            }

            JavaScriptSerializer jss = new JavaScriptSerializer();
            string jsonDoc1 = jss.Serialize(details1);
            string jsonDoc2 = jss.Serialize(details2);

            string jsonDoc = jsonDoc1 + " + " + jsonDoc2;

            return jsonDoc;
        }

        [WebMethod]
        [ScriptMethod]
        public static Object GetOneWay(string origin, string destination, string date, string time)
        {

            string departuresQuery = "";
            
            departuresQuery = "SELECT TOP (10) [AL],[ORIGIN],[DESTINATION1],cast([FLTDATE] as varchar(20)) [FLTDATE] "
                                +",[STD],[STA],[BLOCK],[FNR],[PRICE1],[SPLIT]"
                                +",[TRANZ],[ORIGIN2],[DESTINATION],[WEEKDAYS] "
                                +",[VALIDFROM],[VALIDTO],[DEPARTURE],[ARRIVAL],[DIFF],[AL_IATA]"
                                + ",[FLIGHT NO],[PRICE2],[TOTAL_TIME],[TOTAL_PRICE] "
                                + "FROM ONE_STOP('"+ origin + "','"+ destination + "','"+date+"','"+time+ "') where [TOTAL_PRICE] is not null order by [TOTAL_TIME] asc";
                

            SqlCommand cmdDepartures = new SqlCommand(departuresQuery, conn);

            var details = new List<Dictionary<string, object>>();

            using (SqlDataReader rdr = cmdDepartures.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var dict = new Dictionary<string, object>();

                        for (int i = 0; i < rdr.FieldCount; i++)
                        {
                            dict.Add(rdr.GetName(i), rdr.IsDBNull(i) ? null : rdr.GetValue(i));
                        }

                        details.Add(dict);
                    }
                }
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string jsonDoc = jss.Serialize(details);

            return jsonDoc;

        }



        }
}